<?php
require_once 'API.php';
use PHPUnit\Framework\TestCase;

class APITest extends TestCase
{
    protected $api;
    protected function setUp(): void
    {
        $this->api = new API();
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';
    
        $payload = array(
            'first_name' => 'ANew',
            'middle_name' => 'tester',
            'last_name' => 'Lasted',
            'contact_number' => 126451
        );
    
        $result = json_decode($this->api->httpPost($payload), true);  
        $this->assertIsArray($result); // add this line to verify $result is an array
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }
    
    

    public function testHttpPut()
{
    $_SERVER['REQUEST_METHOD'] = 'PUT';
    $_SERVER['REQUEST_URI'] = '/id/36';
    $payload = array(
        'first_name' => 'Updated',
        'middle_name' => 'updated',
        'last_name' => 'last updated',
        'contact_number' => 555-5678
    );

    $result = json_decode($this->api->httpPut('/id/36', $payload), true);
    $this->assertIsArray($result);
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals($result['status'], 'success');
    $this->assertArrayHasKey('data', $result);
}

   
    

public function testHttpGet()
{
    $_SERVER['REQUEST_METHOD'] = 'GET';
    $_SERVER['REQUEST_URI'] = '/users/123';

    $result = json_decode($this->api->httpGet(), true);
    $this->assertIsArray($result);
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals($result['status'], 'success');
    $this->assertArrayHasKey('data', $result);
}

public function testHttpDelete()
{
    $_SERVER['REQUEST_METHOD'] = 'DELETE';
    $_SERVER['REQUEST_URI'] = '/users/123';

    $result = json_decode($this->api->httpDelete('/users/123', []), true);
    $this->assertIsArray($result);
    $this->assertArrayHasKey('status', $result);
    $this->assertEquals($result['status'], 'success');
    $this->assertArrayHasKey('data', $result);
}


}

?>
